"use strict";

// Available strings for additional languages
// If no language is found, the default language (English) will be displayed
module.exports.general = {

	// 中文
	zh: {
		unknowncommand: "错误输入，输入\"guide help\"查看更多使用帮助",
		helpheader: "输入 \"guide help\" 查看更多使用帮助",
		helpbody: [
			["guide, 副本提示开/关", "PRMSG"],
			["guide gui, 开启模组界面", "PRMSG"],
			["guide voice, 语音提示开/关", "PRMSG"],
			["guide lNotice, 团队通知开/关", "PRMSG"],
			["guide gNotice, 组队通知开/关", "PRMSG"],
			["guide auto~en~zh, 设置语言自动/英文/中文", "PRMSG"],
			["guide male~female, 设置语音声音为男/女性", "PRMSG"],
			["guide 1~10, 调节语音速度10为最快语速", "PRMSG"],
			["guide spawnObject, 地面提示开/关", "PRMSG"],
			["guide stream,  主播模式开/关", "PRMSG"],
			["guide dungeons, 查询目前支持副本", "PRMSG"],
			["guide verbose id, 单独设置指定副本消息开/关", "PRMSG"],
			["guide spawnObject id, 单独设置指定副本地面提示开/关", "PRMSG"],
			["guide cr, 消息颜色: 红色", "CRMSG"],
			["guide co, 消息颜色: 橙色", "COMSG"],
			["guide cy, 消息颜色: 黄色", "CYMSG"],
			["guide cg, 消息颜色: 绿色", "CGMSG"],
			["guide cdb, 消息颜色: 深蓝色", "CDBMSG"],
			["guide cb, 消息颜色: 蓝色", "CBMSG"],
			["guide cv, 消息颜色: 紫色", "CVMSG"],
			["guide cp, 消息颜色: 粉色", "CPMSG"],
			["guide clp, 消息颜色: 淡粉色", "CLPMSG"],
			["guide clb, 消息颜色: 天蓝色", "CLBMSG"],
			["guide cbl, 消息颜色: 黑色", "CBLMSG"],
			["guide cgr, 消息颜色: 灰色", "CGRMSG"],
			["guide cw, 消息颜色: 白色", "CWMSG"]
		],
		red: "红色",
		green: "绿色",
		settings: "设置",
		spawnObject: "地面标记物",
		speaks: "语音消息（TTS）",
		lNotice: "发送消息至团队频道",
		gNotice: "发送消息至组队频道",
		stream: "主播模式",
		language: "选择语言",
		voice: "语音",
		rate: "语音速度",
		color: "消息颜色",
		dungeons: "副本",
		verbose: "消息提示",
		objects: "地面提示",
		test: "测试",
		module: "TERA-Guide",
		enabled: "开",
		disabled: "关",
		male: "男性",
		female: "女性",
		voicetest: "语音测试",
		colorchanged: "颜色已改变",
		dgnotfound: "未找到副本.",
		dgnotspecified: "未指定副本.",
		enterdg: "进入副本",
		fordungeon: "副本"
	}
};